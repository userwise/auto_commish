class ChangeAmountToBalanceForPlayerBalances < ActiveRecord::Migration
  def change
    rename_column :player_balances, :amount, :balance
  end
end
