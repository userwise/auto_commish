class ChangeLeagueFeeToFeeForLeagues < ActiveRecord::Migration
  def change
    rename_column :leagues, :league_fee, :fee
  end
end
