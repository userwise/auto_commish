class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :full_name
      t.string :email
      t.string :phone
      t.integer :balance
      t.integer :league_id

      t.timestamps
    end
  end
end
