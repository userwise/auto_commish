class CreatePlayerBalances < ActiveRecord::Migration
  def change
    create_table :player_balances do |t|
      t.integer :amount
      t.integer :player_id

      t.timestamps
    end
  end
end
