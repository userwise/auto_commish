class AddLeagueFeeToLeague < ActiveRecord::Migration
  def change
    add_column :leagues, :league_fee, :integer
  end
end
