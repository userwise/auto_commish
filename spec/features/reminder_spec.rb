require 'spec_helper'

feature "Sending a payment update email to the entire league" do

  background do
    @league = League.create(name: "stuff", fee: 350)
    @player = Player.create(full_name: "name", league_id: @league.id, email: "233243@example.com", phone: "2404322290")
    visit league_path(@league)
  end

  scenario "email should be sent to members of the league" do
    EmailWorker.stub(:perform_async)

    expect(EmailWorker).to receive(:perform_async).with(@league.id)
    click_link_or_button "Send League Update"
  end
end

feature "Sending reminder text message to individual owner" do

  background do
    @league = League.create(name: "stuff", fee: 350)
    @player = Player.create(full_name: "name", league_id: @league.id, email: "233243@example.com", phone: "2404322290")
    visit league_path(@league)
  end

  xscenario "reminder text message should be sent to owners who still owe" do
    click_link_or_button "send text"
    message = double('message')
    Message.stub(:new).with([@player]).and_return([message])
    message.stub(:send_text_reminder)
    expect(message).to receive(:send_text_reminder)
  end

end
