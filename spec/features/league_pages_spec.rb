require 'spec_helper'

feature "Creating a new league" do

  background do
    visit new_league_path
  end

  scenario "user creates a new league" do

    expect {
      fill_in "League Name", with: "Cru Luv"
      fill_in "League Fee", with: "35000"
      click_link_or_button "Create League"
    }.to change(League, :count).by 1

    within ".alert" do
      expect(page).to have_content "League was successfully created"
    end
  end

  scenario "user attempts to create a league without a league name" do

    expect {
      fill_in "League Name", with: ""
      fill_in "League Fee", with: "35000"
      click_link_or_button "Create League"
    }.to_not change(League, :count).by 1

    within ".alert" do
      expect(page).to have_content "League could not be saved"
    end
  end

  scenario "user attempts to create a league without a league fee" do

    expect {
      fill_in "League Name", with: "Cru Luv"
      fill_in "League Fee", with: ""
      click_link_or_button "Create League"
    }.to_not change(League, :count).by 1

    within ".alert" do
      expect(page).to have_content "League could not be saved"
    end
  end

  scenario "user enters league fee as a string value" do

    expect {
      fill_in "League Name", with: "Cru Luv"
      fill_in "League Fee", with: "notavalue"
      click_link_or_button "Create League"
    }.to_not change(League, :count).by 1

    within ".alert" do
      expect(page).to have_content "League could not be saved"
    end
  end

  scenario "user enters league fee as a negative value" do

    expect {
      fill_in "League Name", with: "Cru Luv"
      fill_in "League Fee", with: "-90"
      click_link_or_button "Create League"
    }.to_not change(League, :count).by 1

    within ".alert" do
      expect(page).to have_content "League could not be saved"
    end
  end

  scenario "user attempts to create a league with name that already exists" do

    League.create(name: "Cru Luv", fee: "300")

    expect {
      fill_in "League Name", with: "Cru Luv"
      fill_in "League Fee", with: "300"
      click_link_or_button "Create League"
    }.to_not change(League, :count).by 1

    within ".alert" do
      expect(page).to have_content "League could not be saved"
    end
  end
end

feature "Add players to an existing" do

  before do
    @league = League.create(name: "Cru Luv", fee: "300")
    visit new_league_player_path(@league)
  end

  scenario "user adds a new player" do

    expect {
      fill_in "Full Name", with: "Kareem Grant"
      fill_in "Email", with: "kgrant@example.com"
      fill_in "Phone", with: "2403339999"
      click_link_or_button "Save"
    }.to change(Player, :count).by 1

    within ".alert" do
      expect(page).to have_content "Player was added"
    end
  end

  scenario "a balance equal to the league is created with each new player" do

    expect {
      fill_in "Full Name", with: "Kareem Grant"
      fill_in "Email", with: "kgrant@example.com"
      fill_in "Phone", with: "2403339999"
      click_link_or_button "Save"
    }.to change(PlayerBalance, :count).by 1

    within ".alert" do
      expect(page).to have_content "Player was added"
    end

    expect(Player.last.player_balance.balance_in_dollars).to eq -300
  end

  scenario "user attempts to add player with an existing email address" do
    Player.create(full_name: "Wolverine", email: "wolverine@example.com",
                  phone: "1112223333", league_id: @league.id)
    expect {
      fill_in "Full Name", with: "Kareem Grant"
      fill_in "Email", with: "wolverine@example.com"
      fill_in "Phone", with: "2403339999"
      click_link_or_button "Save"
    }.to_not change(Player, :count).by 1

    within ".alert" do
      expect(page).to have_content "Player could not be added"
    end

  end

  scenario "user attempts to add player with an existing phone number" do

    Player.create(full_name: "Wolverine", email: "wolverine@example.com",
                  phone: "1112223333", league_id: @league.id)
    expect {
      fill_in "Full Name", with: "Kareem Grant"
      fill_in "Email", with: "notthesame@example.com"
      fill_in "Phone", with: "1112223333"
      click_link_or_button "Save"
    }.to_not change(Player, :count).by 1

    within ".alert" do
      expect(page).to have_content "Player could not be added"
    end
  end
end

feature "Displaying leagues" do

  scenario "list of leagues is displayed and with to show page for each league" do
    l1 = League.create(name: "Cru Luv", fee: "300")
    l2 = League.create(name: "G.I. Joe", fee: "300")
    l3 = League.create(name: "Jumpstart Lab", fee: "300")
    visit leagues_path

    expect(page).to have_link("Cru Luv", href: league_path(l1))
    expect(page).to have_link("G.I. Joe", href: league_path(l2))
    expect(page).to have_link("Jumpstart Lab", href: league_path(l3))
  end
end

feature "Displaying players" do

  before do
    @l1 = League.create(name: "Cru Luv", fee: "300")
    @p1 = @l1.players.create(full_name: "Kevin Durant", email: "kdurant@example.com", phone: "111222333")
    @p2 = @l1.players.create(full_name: "Russell Westbrook", email: "rwestbrook@example.com", phone: "111223333")
    visit league_path(@l1)
  end

  scenario "list of players should be displayed with a link to edit the player's details" do

    expect(page).to have_content "Kevin Durant"
    expect(page).to have_content "Russell Westbrook"
    expect(page).to have_link "edit", href: edit_league_player_path(@l1, @p1)
  end

end

feature "Editing exiting players for a specific league" do

  before do
    @l1 = League.create(name: "Cru Luv", fee: "300")
    @p1 = @l1.players.create(full_name: "Kevin Durant", email: "kdurant@example.com", phone: "111222333")
    visit league_path(@l1)
  end

  scenario "navigate to a player's edit page" do
    click_link_or_button "player-#{@p1.id}"
    expect(page).to have_content "Edit Player"

    fill_in "Full Name", with: "Kareem Grant"
    fill_in "Email", with: "notthesame@example.com"
    fill_in "Phone", with: "1112223377"
    click_link_or_button "Save"

    within ".table" do
      expect(page).to have_content "1112223377"
    end

    within ".alert" do
      expect(page).to have_content "Player was successfully updated"
    end
  end
end

