require 'spec_helper'

feature "Recording a payment" do

  background do
    @league = League.create(name: "stuff", fee: 350)
    @player = Player.create(full_name: "name", league_id: @league.id, email: "233243@example.com", phone: "2404322290")
    visit league_player_path(@league, @player)
  end

  scenario "player balance should decrease by the payment amount" do
    click_link_or_button "Record Payment"
    fill_in "payment-amount", with: "300"
    click_link_or_button "Record Payment"

    expect(@player.balance).to eq -50
  end

  scenario "payment will be recorded and listed in player show view" do
    expect {
      click_link_or_button "Record Payment"
      fill_in "payment-amount", with: "300"
      click_link_or_button "Record Payment"
    }.to change(Payment, :count).by 1

    within ".payments-table" do
      expect(page).to have_content "300"
    end
  end

end
