# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :player_balance do
    amount 1
    player_id 1
  end
end
