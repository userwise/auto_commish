# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :player do
    full_name "Kareem Grant"
    email "kgrant@example.com"
    phone "2404449999"

    league
  end
end
