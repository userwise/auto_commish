# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :league do
    name "New Millionaires"
    fee "35000"
  end
end
