class TextMessageWorker
  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform
    League.all.each do |league|
      league.gms_who_owe.each do |player|
        Message.new(player).send_text_reminder
      end
    end
  end
end
