class EmailWorker
  include Sidekiq::Worker
  sidekiq_options queue: "low"

  def perform(league_id)
    league = League.find(league_id)
    PlayerMailer.group_update(league).deliver
  end

end
