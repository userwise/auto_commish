class GroupEmailWorker
  include Sidekiq::Worker
  sidekiq_options queue: "high"

  def perform
    League.all.each do |league|
      PlayerMailer.group_update(league).deliver
    end
  end
end

