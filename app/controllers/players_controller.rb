class PlayersController < ApplicationController

  before_action :find_player, only: [:show, :edit, :send_text, :update]

  def show
    @league = @player.league
    @payments = @player.player_balance.payments
  end

  def new
    @league = League.find(params[:league_id])
    @player = Player.new
  end

  def create
    @league = League.find(params[:league_id])
    @player = @league.players.build(player_params)

    if @player.save
      redirect_to @player.league, notice: "Player was added"
    else
      flash.now[:alert] = "Player could not be added"
      render :new
    end
  end

  def edit
    @league = @player.league
  end

  def update
    if @player.update(player_params)
      redirect_to @player.league, notice: "Player was successfully updated"
    end
  end

  def send_text
    Message.new(@player).send_text_reminder
    redirect_to @player.league, notice: "Text was successfully sent"
  end


  private

  def player_params
    params.require(:player).permit(:full_name, :email, :phone, :league_id)
  end

  def find_player
    @player = Player.find(params[:id])
  end
end

