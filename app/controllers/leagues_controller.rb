class LeaguesController < ApplicationController
  def index
    @leagues = League.all
  end

  def show
    @league = League.find(params[:id])
  end

  def new
    @league = League.new
  end

  def create
    @league = League.new(league_params)

    if @league.save
      redirect_to @league, notice: "League was successfully created"
    else
      flash.now[:alert] = "League could not be saved"
      render :new
    end
  end

  def send_update
    league = League.find(params[:id])
    EmailWorker.perform_async(league.id)
    redirect_to league, notice: "Payment update was sent"
  end

  def send_text_reminders
    TextMessageWorker.perform_async
    redirect_to leagues_path, notice: "Text messages were sent"
  end

  private

  def league_params
    params.require(:league).permit(:name, :fee)
  end
end
