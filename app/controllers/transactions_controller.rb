class TransactionsController < ApplicationController

  before_action :get_player, only: [:new, :create]

  def new
    @league = @player.league
  end

  def create
    if Transaction.new(@player).update_balance(params[:amount].to_i)
      redirect_to league_player_path(@player.league, @player), notice: "Payment successfully recorded"
    end
  end

  private

  def get_player
    @player = Player.find(params[:player_id])
  end
end
