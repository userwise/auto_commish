class PlayerMailer < ActionMailer::Base
  default from: "kareem.grant@gmail.com"

  def group_update(league)

    @league = league

    recipients = @league.players.pluck(:email)

    mail(to: recipients.join(", "),
         subject: "#{league.name} Payment Update -  #{DateTime.now.to_formatted_s(:long_ordinal)}")
  end

end
