class PlayerBalance < ActiveRecord::Base
  belongs_to :player
  has_many :payments

  before_create :convert_balance_to_cents

  def balance_in_dollars
    balance.to_f / 100 if balance
  end

  def update_balance(balance)
    update_column(:balance, balance.to_f * 100)
  end

  private

  def convert_balance_to_cents
    self.balance = balance.to_f * 100 if balance.present?
  end
end
