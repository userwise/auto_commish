class Message

  attr_reader :player

  def initialize(player)
    @player = player
  end


  def send_text_reminder
    number_to_send_to = player.phone

    twilio_sid = ENV["TWILIO_SID"]
    twilio_token = ENV["TWILIO_TOKEN"]
    twilio_phone_number = "12402930574"

    @twilio_client = Twilio::REST::Client.new twilio_sid, twilio_token

    @twilio_client.account.sms.messages.create(
      :from => "+1#{twilio_phone_number}",
      :to => number_to_send_to,
      :body => "This is reminder that you currently owe $#{-player.balance} for 2012-2013 #{player.league.name} Fantasy Basketball Season, please make your payment as soon as possible")
  end

end
