class Transaction

  def initialize(player)
    @player = player
  end

  def update_balance(amount)
    new_balance = @player.balance + amount

    if @player.player_balance.update_balance(new_balance)
      @player.player_balance.payments.create(amount: amount)
    else
      false
    end
  end

end
