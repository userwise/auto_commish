class Player < ActiveRecord::Base
  after_create :set_initial_player_balance

  belongs_to :league
  has_one :player_balance

  validates :full_name, presence: true
  validates :email, presence: true
  validates :email, uniqueness: {case_sensitive: true}

  validates :phone, presence: true
  validates :phone, uniqueness: true

  validates :league_id, presence: true


  def balance
    @balance ||= player_balance.balance_in_dollars
  end

  private

  def set_initial_player_balance
    PlayerBalance.create(balance: -league.fee, player_id: self.id)
  end
end
