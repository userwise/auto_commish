class League < ActiveRecord::Base
  validates :name, presence: true
  validates :name, uniqueness: {case_sensitive: true}
  validates :fee, presence: true
  validates_numericality_of :fee, greater_than: 0
  has_many :players, -> {includes :player_balance }

  def gms_who_won
    @gms_who_won ||= players.select do |player|
      player.player_balance.balance > 0
    end.sort_by{|p| p.balance}.reverse
  end

  def gms_who_paid
    @gms_who_paid ||= players.select do |player|
      player.player_balance.balance == 0
    end.sort_by{|p| p.balance}.reverse
  end

  def gms_who_owe
    @gms_who_owe ||= players.select do |player|
      player.player_balance.balance < 0
    end.sort_by{|p| p.balance}.reverse
  end

end
