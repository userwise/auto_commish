class Payment < ActiveRecord::Base

  before_create :convert_amount_to_cents

  def amount_in_dollars
    amount.to_f / 100 if amount
  end

  private

  def convert_amount_to_cents
    self.amount = amount.to_f * 100 if amount.present?
  end
end
