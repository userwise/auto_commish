require 'sidekiq/web'

AutoCommish::Application.routes.draw do
  root 'leagues#index'
  resources :leagues, only: [:index, :new, :create, :show] do

    member do
      get :send_update
    end

    collection do
      get :send_text_reminders
    end

    resources :players, only: [:new, :show, :create, :edit, :update] do

      member do
        get :send_text
      end

      resources :transactions, only: [:new, :create]
    end
  end

  mount Sidekiq::Web, at: "/sidekiq"
end
