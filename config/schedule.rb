set :output, "#{path}/log/cron.log"

every :day, :at => '10:00am' do
  runner 'GroupEmailWorker.perform_async', :environment => "development"
end

every :day, :at => '10:02am' do
  runner 'TextMessageWorker.perform_async', :environment => "development"
end

